class Item {
  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items = []) {
    this.items = items;
  }

  updateQuality() {
    for (let i = 0; i < this.items.length; i++) {
      const item = this.items[i];

      if (item.name == 'Sulfuras, Hand of Ragnaros') {
        continue;
      }

      if (item.name == 'Aged Brie') {
        this.agedBrieUpdateQuality(item);
      } else if (item.name == 'Backstage passes to a TAFKAL80ETC concert') {
        this.backstageUpdateQuality(item);
      } else if (item.name == 'Conjured Mana Cake') {
        this.conjuredUpdateQuality(item);
      } else {
        this.normalUpdateQuality(item);
      }
    }
  }

  conjuredUpdateQuality(item) {
    if (item.quality > 0) item.quality -= 2;
    item.sellIn--;
    if (item.sellIn < 0) item.quality -= 2;

    if (item.quality < 0) item.quality = 0;
  }

  normalUpdateQuality(item) {
    if (item.quality > 0) item.quality--;
    item.sellIn--;
    if (item.sellIn < 0 && item.quality > 0) item.quality--;
  }



  backstageUpdateQuality(item) {
    if (item.quality < 50) {
      item.quality++;
    }

    if (item.quality < 50) {
      if (item.sellIn < 11) item.quality++;
      if (item.sellIn < 6) item.quality++;
    }

    item.sellIn--;
    if (item.sellIn < 0) item.quality = 0;
  }

  agedBrieUpdateQuality(item) {
    if (item.quality < 50) item.quality++;
    item.sellIn--;
    if (item.sellIn < 0 && item.quality < 50) item.quality++;
  }
}

class LegacyShop {
  constructor(items = []) {
    this.items = items;
  }

  updateQuality() {
    for (var i = 0; i < this.items.length; i++) {
      if (this.items[i].name != 'Aged Brie' && this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
        if (this.items[i].quality > 0) {
          if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
            this.items[i].quality = this.items[i].quality - 1;
          }
        }
      } else {
        if (this.items[i].quality < 50) {
          this.items[i].quality = this.items[i].quality + 1;
          if (this.items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
            if (this.items[i].sellIn < 11) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
            if (this.items[i].sellIn < 6) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
          }
        }
      }
      if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
        this.items[i].sellIn = this.items[i].sellIn - 1;
      }
      if (this.items[i].sellIn < 0) {
        if (this.items[i].name != 'Aged Brie') {
          if (this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
            if (this.items[i].quality > 0) {
              if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
                this.items[i].quality = this.items[i].quality - 1;
              }
            }
          } else {
            this.items[i].quality = this.items[i].quality - this.items[i].quality;
          }
        } else {
          if (this.items[i].quality < 50) {
            this.items[i].quality = this.items[i].quality + 1;
          }
        }
      }
    }
  }
}

const createItems = () => [
  new Item("+5 Dexterity Vest", 10, 20),
  new Item("Aged Brie", 2, 0),
  new Item("Elixir of the Mongoose", 5, 7),
  new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
  new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
  new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49)
];

module.exports = {
  Item,
  Shop,
  LegacyShop,
  createItems
};
