const {Shop, LegacyShop, Item, createItems} = require('../src/gilded_rose.js');

describe("Gilded Rose", function() {
  
  it("should continue to work as before", function() {

    const shop = new Shop(createItems());
    const legacy = new LegacyShop(createItems());

    for (let i = 0; i < 50; i++) {
      legacy.updateQuality();
      shop.updateQuality();

      expect(shop.items.map(i => i.name)).toEqual(legacy.items.map(i => i.name));
      expect(shop.items.map(i => i.sellIn)).toEqual(legacy.items.map(i => i.sellIn));
      expect(shop.items.map(i => i.quality)).toEqual(legacy.items.map(i => i.quality));
    }
  });

});

describe("Conjured items", function() {

  it("should decrease quality", function() {
    const shop = new Shop([ new Item("Conjured Mana Cake", 0, 1) ]);
    shop.updateQuality();
    expectQualityToBe(shop, 0);
  });

  it("should not decrease quality if quality is zero", function() {
    const shop = new Shop([ new Item("Conjured Mana Cake", 0, 0) ]);
    shop.updateQuality();
    expectQualityToBe(shop, 0);
  });

  it("should not have negative quality", function() {
    const shop = new Shop([ new Item("Conjured Mana Cake", 0, 2) ]);
    shop.updateQuality();
    expectQualityToBe(shop, 0);
  });

  it("should decrease sell in", function() {
    const shop = new Shop([ new Item("Conjured Mana Cake", 0, 2) ]);
    shop.updateQuality();
    expect(shop.items[0].sellIn).toBe(-1);
  });

  it("should decrease quality another time if sell in is lower than zero", function() {
    const shop = new Shop([ new Item("Conjured Mana Cake", -1, 4) ]);
    shop.updateQuality();
    expectQualityToBe(shop, 0);
  });

  function expectQualityToBe(shop, expected) {
    expect(shop.items[0].quality).toBe(expected);
  }

});
